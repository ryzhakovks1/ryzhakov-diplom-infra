resource "yandex_kubernetes_cluster" "regional_cluster_ryzhakovks" {
  name        = "ryzhakovks-cluster"
  description = "cluster for netology diploma"

  network_id = yandex_vpc_network.network-1.id

  master {
    regional {
      region = "ru-central1"

      location {
        zone      = yandex_vpc_subnet.prod.zone
        subnet_id = yandex_vpc_subnet.prod.id
      }

      location {
        zone      = yandex_vpc_subnet.preprod.zone
        subnet_id = yandex_vpc_subnet.preprod.id
      }

      location {
        zone      = yandex_vpc_subnet.dev.zone
        subnet_id = yandex_vpc_subnet.dev.id
      }
    }

    public_ip = true

    master_logging {
      enabled                    = true
      folder_id                  = var.yc_folder_id
      kube_apiserver_enabled     = true
      cluster_autoscaler_enabled = true
      events_enabled             = true
      audit_enabled              = true
    }
  }

  service_account_id      = yandex_iam_service_account.k8s-sa.id
  node_service_account_id = yandex_iam_service_account.ryzhakovks.id

  release_channel = "STABLE"
}
resource "yandex_kubernetes_node_group" "prod_node_group" {
  cluster_id  = yandex_kubernetes_cluster.regional_cluster_ryzhakovks.id
  name        = "prod-nodes"
  description = "prod nodes"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat        = true
      subnet_ids = ["${yandex_vpc_subnet.prod.id}"]
    }

    resources {
      memory = 6
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = yandex_vpc_subnet.prod.zone
    }
  }
}

resource "yandex_kubernetes_node_group" "preprod_node_group" {
  cluster_id  = yandex_kubernetes_cluster.regional_cluster_ryzhakovks.id
  name        = "preprod-nodes"
  description = "preprod nodes"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat        = true
      subnet_ids = ["${yandex_vpc_subnet.preprod.id}"]
    }

    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = yandex_vpc_subnet.preprod.zone
    }
  }
}

resource "yandex_container_registry" "ryzhakovks-diploma" {
  name      = "diploma-registry"
  folder_id = var.yc_folder_id

}

resource "yandex_kubernetes_node_group" "dev_node_group" {
  cluster_id  = yandex_kubernetes_cluster.regional_cluster_ryzhakovks.id
  name        = "dev-nodes"
  description = "dev nodes"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat        = true
      subnet_ids = ["${yandex_vpc_subnet.dev.id}"]
    }

    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = yandex_vpc_subnet.dev.zone
    }
  }
}
