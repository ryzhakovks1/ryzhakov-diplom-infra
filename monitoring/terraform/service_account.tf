## Create SA
resource "yandex_iam_service_account" "k8s-sa" {
  folder_id = var.yc_folder_id
  name      = "k8s-sa"
}

## Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "sa-k8s-editor" {
  folder_id = var.yc_folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

## Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "sa-k8s-static-key" {
  service_account_id = yandex_iam_service_account.k8s-sa.id
  description        = "static access key for sa-k8s-editor"
}

resource "yandex_iam_service_account" "ryzhakovks" {
  folder_id = var.yc_folder_id
  name      = "ryzhakovks"
}

## Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "sa-k8s-node-push" {
  folder_id = var.yc_folder_id
  role      = "container-registry.images.pusher"
  member    = "serviceAccount:${yandex_iam_service_account.ryzhakovks.id}"
}
resource "yandex_resourcemanager_folder_iam_member" "sa-k8s-node-pull" {
  folder_id = var.yc_folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.ryzhakovks.id}"
}
resource "yandex_container_registry_iam_binding" "sa-k8s-registry-push" {
  registry_id = yandex_container_registry.ryzhakovks-diploma.id
  role        = "container-registry.images.pusher"
  members     = ["serviceAccount:${yandex_iam_service_account.ryzhakovks.id}"]
}
resource "yandex_container_registry_iam_binding" "sa-k8s-registry-pull" {
  registry_id = yandex_container_registry.ryzhakovks-diploma.id
  role        = "container-registry.images.puller"
  members     = ["serviceAccount:${yandex_iam_service_account.ryzhakovks.id}"]
}
## Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "sa-k8s-node-static-key" {
  service_account_id = yandex_iam_service_account.ryzhakovks.id
  description        = "static access key for object storage"
}
resource "yandex_iam_service_account_key" "sa-registry-key" {
  service_account_id = yandex_iam_service_account.ryzhakovks.id
}
