# Дипломный практикум в Yandex.Cloud
  * [Цели:](#цели)
  * [Этапы выполнения:](#этапы-выполнения)
     * [Создание облачной инфраструктуры](#создание-облачной-инфраструктуры)
     * [Создание Kubernetes кластера](#создание-kubernetes-кластера)
     * [Создание тестового приложения](#создание-тестового-приложения)
     * [Подготовка cистемы мониторинга и деплой приложения](#подготовка-cистемы-мониторинга-и-деплой-приложения)
     * [Установка и настройка CI/CD](#установка-и-настройка-cicd)

## Цели:

1. Подготовить облачную инфраструктуру на базе облачного провайдера Яндекс.Облако.
2. Запустить и сконфигурировать Kubernetes кластер.
3. Установить и настроить систему мониторинга.
4. Настроить и автоматизировать сборку тестового приложения с использованием Docker-контейнеров.
5. Настроить CI для автоматической сборки и тестирования.
6. Настроить CD для автоматического развёртывания приложения.

## Этапы выполнения:

### Рабочая среда

  - Машина Ubuntu23 7.4.1, terraform 1.5.5, Yandex Cloud CLI 0.115.0
  - Машина Ubuntu 22.04.3, helm 3.14.2, terraform 1.5.5, kubectl 1.28.2, Yandex Cloud CLI 0.115.0

 2. Создаем сервисный аккаунт для управления инфраструктурой 

    ```powershell
    yc iam service-account create --name ryzhakovks-diploma-netology
    yc iam service-account list
    yc resource-manager folder add-access-binding *********lg4baptcj0 --role admin --subject serviceAccount:*********lg4baptcj0
    yc iam key create --service-account-id *********off8gunls --folder-name netology --output key.json
    yc config profile create ryzhakovks-diploma-netology


    yc config set service-account-key key.json
    yc config set cloud-id *********9srl0rsq057
    yc config set folder-id *********9srl0rsq057

    export YC_TOKEN=$(yc iam create-token)
    export YC_CLOUD_ID=$(yc config get cloud-id)
    exportYC_FOLDER_ID=$(yc config get folder-id)
    ```

3. Создал бакет для хранения состояния инфраструктуры создаваемой Terraform
    Написал манифесты для создания [bucket](./terraform/bucket/bucket.tf) и [сервисного аккаунта](./terraform/bucket/service_account.tf) и ключей для него

 ```
 terraform init
 terraform fmt
 terraform validate
 terraform plan
 terraform apply
 
 ```

4. Ключи записываем в переменные

    ```powershell
    export AWS_ACCESS_KEY_ID="YCAJEkaF****hzaNakr-GUAME"
    export AWS_SECRET_ACCESS_KEY="YCOB1fO64pZL*****72DtcZf9iWWu8xruWEVWNcM"
    ```

--- 

### Создание Kubernetes кластера

Клстер содаётся в [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/services/managed-kubernetes)

- [Сети](./terraform/network.tf)
- [Сервис аккаунты и ключи](./terraform/service_account.tf)
- [K8s кластер, группы нод и докер реестр](./terraform/main.tf)
- В [Provider](/terraform/provider.tf) прописал созданный ранее бакет для хранения    состояния


Создание кластера 

 ```
 terraform init
 terraform fmt
 terraform validate
 terraform plan
 terraform apply
 
 ```

Проверяем результат в yc

![cluster](./img/cluster.png)

Подключаемся к кластеру и проверяем результат

![cluster00](./img/cluster00.png)
![cluster1](./img/cluster1.png)

Так же проверяем что в бакете сохранилось состояние terraform

![cluster01](./img/cluster01.png)

### Создание тестового приложения

1. Создал [репозиторий](https://gitlab.com/ryzhakovks1/ryzhakov-app) с тестовым приложением

2. Сделал простенький [Dockerfile](https://gitlab.com/ryzhakovks1/ryzhakov-app/-/blob/main/Dockerfile?ref_type=heads)

    ```Dockerfile
    FROM nginx:1.25.4

    COPY html /usr/share/nginx/html/
    ```

3. Написал [манифест](https://gitlab.com/ryzhakovks1/ryzhakov-app/-/blob/main/diploma-manifest.yaml.tmp?ref_type=heads)

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: diploma-app-deployment
  namespace: diploma-app
spec:
  replicas: 2
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 50%
      maxUnavailable: 50%
  selector:
    matchLabels:
      app: diploma-app-deployment
  template:
    metadata:
      labels:
        app: diploma-app-deployment
    spec:
      containers:
      - name: diploma-app
        image: cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG
        resources:
          limits:
            memory: 512Mi
            cpu: "0.5"
          requests:
            memory: 256Mi
            cpu: "0.25"
        ports:
        - containerPort: 80
...
---
apiVersion: v1
kind: Service
metadata:
  name: diploma-app-svc
  namespace: diploma-app
spec:
  type: LoadBalancer
  selector:
    app: diploma-app-deployment
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80

```
Создание неймспеса для приложения 

    ```
    kubectl create ns diploma-app
    ```
1. Для мониторинга используем оператор [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus). 
Так как K8s кластер у меня версии 1.26, я взял ветку [release-0.13](https://github.com/prometheus-operator/kube-prometheus/tree/release-0.13) и скопировал манифесты себе в папку [monitoring](./monitoring/)

2. Так же я написал [манифест](./monitoring/grafana-service.yaml) с сервисом типа LoadBalancer для доступа к Grafana извне

3. Применяем манифесты

    ```bash
    kubectl apply --server-side -f manifests/setup
    kubectl wait --for condition=Established --all CustomResourceDefinition --namespace=monitoring
    kubectl apply -f manifests/
    kubectl apply -f grafana-service.yaml
    ```
провереям интерфейс 

![mon1](./img/mon1.png)

![mon1](./img/mon2.png)
http://158.160.153.238:3000/login


### Установка и настройка CI/CD

Для конвеера был выбран [gitlab.com](https://gitlab.com)

Создаем раннер в [репозитории](https://gitlab.com/ryzhakovks1/ryzhakov-app) приложения, копируем токен

![runner1](runner001.png)
![runner2](runner002.png)

В нашем кластере создаем неймспейс и [секрет](./k8s/gitlab-runner-secret.yaml) с токеном

    ```bash
    cd k8s/
    kubectl create ns gitlab-runner
    sed -i 's/REDACTED/'"$(echo "glrt-zYfz******L2PR2oznW2q" | base64)"'/g' gitlab-runner-secret.yaml
    kubectl apply --namespace gitlab-runner -f gitlab-runner-secret.yaml
    ```
Далее создаем configmap для доступа к docker registry из раннера.
    ```bash
    cd ../terraform/

    yc iam key create --service-account-name ryzhakovks -o docker_registry_key.json
    
    cat docker_registry_key.json | docker login \
      --username json_key \
      --password-stdin \
      cr.yandex

    kubectl create configmap docker-client-config --namespace gitlab-runner --from-file ~/.docker/config.json
    ```


![map](./img/map.png)

Следующим шагом применяем helm chart с поднятием раннеров у нас в кластере

    ```bash
    cd ../helm
    helm repo add gitlab https://charts.gitlab.io
    helm repo update gitlab
    helm install --namespace gitlab-runner gitlab-runner -f ./values.yaml gitlab/gitlab-runner
    ```

провереям что он подключился и соедянем с нашим клаестром    
![runner3](./img/runner3.png)
![runner2](./img/runner2.png)

Раннер готов, теперь собираем и пушим в реестр наш образ для деплоя в k8s

    ```bash
    cd ../k8s
    docker build -t cr.yandex/crp8itk07jnp5jfa7jgl/kubectl:1.29.2-debian-12-r3-gettext .
    docker push cr.yandex/crp8itk07jnp5jfa7jgl/kubectl:1.29.2-debian-12-r3-gettext
    ```

![image](./img/image.png)

Деламем [.gitlab-ci.yml](https://gitlab.com/ryzhakovks1/ryzhakov-app/-/blob/main/.gitlab-ci.yml?ref_type=heads)
```
stages:
  - build
  - deploy

default:
  image: docker:25.0.3
  tags: 
    - docker
  services:
    - name: docker:25.0.3-dind
      command: ["--tls=false"]
      alias: thedockerhost

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://thedockerhost:2375
  DOCKER_TLS_CERTDIR: ""
  DOCKER_IMAGE_TAG: $CI_JOB_ID

build:
  before_script:
    - apk add --update --no-cache git
  rules:
    - if: '$CI_COMMIT_TAG'
      variables:
        DOCKER_IMAGE_TAG: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "push"
  stage: build
  script:
    - docker login   --username oauth   --password $CI_REGISTRY_PASSWORD  cr.yandex
    - docker build -t cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG .
    - docker push cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG

deploy:
  rules:
    - if: '$CI_COMMIT_TAG'
      variables:
        DOCKER_IMAGE_TAG: $CI_COMMIT_TAG
  image: 
    name: cr.yandex/$REGISTRY_ID/kubectl:1.29.2-debian-12-r3-gettext
    entrypoint: [""]
  stage: deploy
  script:
    - kubectl config get-contexts
    - kubectl config use-context ryzhakovks1/ryzhakov-app:ryzhakovks-diploma
    - envsubst < diploma-manifest.yaml.tmp > diploma-manifest.yaml
    - kubectl apply -f diploma-manifest.yaml

```

Пушим коммит и проверям чтоб начался билд и образ запушился к нас регистр

![commit.png](./img/commit.png)
![push.png](./img/push.png)

Проверяем что под создан
![pod.png](./img/pod.png)

Идем по внешней [ссылке](http://158.160.157.163/)
![done.png](./img/done.png)

  

# TERRAFORM PIPLINE
1. Делаем [сиай](./terraform/terraform.gitlab-ci.yml) при коммите и мердже в мастер он будет запускаться 
    
2. Создаем ветку, коммитем, мерджми 
![merge.png](./img/merge.png)

3. Запускается пайплайн 
    
![job.png](./img/job.png)

![job2.png](./img/job2.png)



