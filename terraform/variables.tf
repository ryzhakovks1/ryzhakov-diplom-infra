# Заменить на ID своего облака
# https://console.cloud.yandex.ru/cloud?section=overview
variable "yandex_cloud_id" {
  default = "b1gmn0ev19srl0rsq057"
}

# Заменить на Folder своего облака
# https://console.cloud.yandex.ru/cloud?section=overview
variable "yandex_folder_id" {
  default = "b1gs48u29hlg4baptcj0"
}


variable "yc_token" {
  type        = string
  description = "Yandex Cloud API key"
  default     = "$yc_token"
}

variable "yc_cloud_id" {
  type        = string
  description = "Yandex Cloud id"
  default     = "$yc_cloud_id"
}

variable "yc_folder_id" {
  type        = string
  description = "Yandex Cloud folder id"
  default     = "$yc_folder_id"
}


